#ifndef DTYPE_H
#define DTYPE_H
//主体类型

//导出导入符号
#ifdef _OS_WIN_
#	define Export __declspec(dllexport) 
#	define Import __declspec(dllimport) 
#elif _OS_LINUX_
#	define Export __attribute__((visibility("default")))
#	define Import 
#endif

#ifdef _INTERNAL_
#	define EXPORT
#else
#	define EXPORT __declspec(dllexport) 
#endif


//操作系统类型
#define OS_WIN 0x80000000
#define OS_LINUX 0x40000000
#define OS_UNIX 0x20000000
#define OS_HA 0x08000000
#define OS_YS 0x04000000
#define OS_ALL (OS_WIN|OS_LINUX|OS_UNIX|OS_HA|OS_YS)

//链接通用GUI库
#ifdef _UNICODE_
#	ifdef _OS_WIN_
#		ifdef _GUI_
#			pragma comment( linker, "/subsystem:windows /entry:mainCRTStartup" )//不显示控制台
#		endif
#	endif
#endif

#ifdef _OS_WIN_
#	if defined _I386_
#		pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#	elif defined _X64_
#		pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#	else
#		pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#	endif
#else //Linux
#endif // _WIN32

//核心类型头
#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define true 1 //逻辑假
#define false 0 //逻辑假
#define not -1 //逻辑未知，实际上也为逻辑真，仅用作状态标志
	
//三值类型（真、假、未知）
typedef signed char terval;
#ifndef __cplusplus
	typedef unsigned char byte;
#endif // __cplusplus
typedef signed char sbyte;

typedef signed char int8;
typedef signed short int16;
typedef signed int int32;
typedef signed long long int64;
typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;
typedef float float32;
typedef double float64;
//C字符串类型
typedef unsigned char* cstring;

#ifndef __cplusplus
	//逻辑类型
	typedef signed char bool;
#endif

#ifdef _WIN32
	//字符串类型
	typedef uint16* string;
	//宽字符类型
	typedef uint16 wchar;
#elif _LINUX
	typedef uint32* string;
	typedef uint32 wchar;
#else
	typedef uint8* string;
	typedef uint8 wchar;
#endif // _WIN32



#ifdef _OS_WIN_
#	define UNI_ENCODING_CONST 2 /*字符编码字节长度常数*/
#elif _OS_LINUX_
#	define UNI_ENCODING_CONST 4 /*字符编码字节长度常数*/
#endif

#ifdef _UNICODE_
#	define T(x) L##x
#	define t(x) L##x
#else
#	define T(x) x
#	define t(x) x
#	ifdef UNI_ENCODING_CONST
#		define UNI_ENCODING_CONST 1 /*字符编码字节长度常数*/
#	endif
#endif // _UNICODE_

//指针类型
typedef void* ptr;
//空指针
#define null ((ptr)0)

#ifdef _64BIT_
	//指针大小
#	define PTR_SIZE 8
	//平台化有符号整数
	typedef int64 nint;
	//平台化无符号整数
	typedef uint64 nuint;
#elif _32BIT_ 
	//指针大小
#	define PTR_SIZE 4
	//平台化有符号整数
	typedef int32 nint;
	//平台化无符号整数
	typedef uint32 nuint;
#endif // _64BIT_

//指针转为平台化无符号整数值
#define PtrToNUInt(x) ((nuint)x)
//平台化无符号整数值转为指针
#define NUIntToPtr(x) ((ptr)x)
//指针到结构体
#define PtrToStruct(p,sut) ((sut)p)

//基础结构体
#define STRUCT(name) typedef struct S_##name name##_S, name;\
struct S_##name

//指针类型结构体
#define STRUCT_P(name) typedef struct S_##name name##_S, *name;\
struct S_##name

//基础共用体
#define UNION(name) typedef union U_##name name##_U, name;\
union U_##name

//计算 STRUCT STRUCT_P 声明的结构体大小
#define STRUCT_SIZE(name) sizeof(name##_S)

//Unicode字符编码数组
STRUCT_P(UniStrArray)
{
	//字符串数量
	int32 count;
	//字符串
	int32 unicode[];
};

//柔性字节数组
STRUCT_P(FlxArray)
{
	uint32 len; //数组字节长度
	byte value[]; //数组值
};

//链表数组
STRUCT_P(LinkArray)
{
	uint32 len;// Data元素数量
	uint32 elmt;// 单个元素大小
	ptr data;// 元素首地址
	LinkArray next;// 下一个数组
};

//版本信息
STRUCT(VersionNumber)
{
	uint16 Major;//主版本
	uint16 Minor;//次版本
	uint16 Revision;//修订版本
	uint16 Build;//构建版本号
};

#define CL_CALL __cdecl	//C语言调用模式
#define STD_CALL __stdcall	//通用调用模式

#define STD_API(type) __declspec(dllexport) type __stdcall //标准调用方式
#define CL_API(type) __declspec(dllexport) type __cdecl //C语言调用方式

#define FUNC_TYPEDEF(name,ret,...) typedef ret (*name)(__VA_ARGS__) //定义有返回值函数类型
#define ACTION_TYPEDEF(name,...) typedef void (*name)(__VA_ARGS__) //定义无返回值函数类型

#define FUNC_PTR(name,ret,...) ret (*name)(__VA_ARGS__) //定义有返回值函数指针
#define ACTION_PTR(name,...) void (*name)(__VA_ARGS__) //定义无返回值函数指针

//可变参数支持
typedef byte* VARG_LIST; //可变参数列表
#ifdef _64BIT_
#	define __VARG_SIZEOF_(type) ((sizeof(type) + 7) & ~ 7)
#elif _32BIT_ 
#	define __VARG_SIZEOF_(type) ((sizeof(type) + 3) & ~ 3)
#endif // _64BIT_

#define VARG_INIT(ap,v) (ap = (byte*)&v + __VARG_SIZEOF_(v)) //初始化可变参数 
#define VARG_NEXT(ap,type) (*(type*)((ap += __VARG_SIZEOF_(type)) - __VARG_SIZEOF_(type))) //获取下一个可变参数 
#define VARG_FREE(ap) (ap = null) //可变参数结束 

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !DTYPE_H
